#!/bin/bash

if [ ! -f "/etc/puppet/fileserver.conf" ]; then
cat > /etc/puppet/fileserver.conf <<EOL
[files]
    path /etc/puppet/files
    allow *
EOL
fi
