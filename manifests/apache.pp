include server

class { 'apache':
    ssl => true,
}

apache::vhost { 'example.com': 
    server_admin    => 'spam@example.com',
    certsrc         => 'puppet:///files/example.com.crt',
    keysrc          => 'puppet:///files/example.com.key',
}
apache::vhost { 'example2.com': 
    server_admin    => 'spam@example2.com',
}
